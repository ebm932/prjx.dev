import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Layout/Home/Home";
import About from "./components/Layout/About/About";
import Footer from "./components/Footer/Footer";
import Contact from "./components/Layout/Contact/Contact";
import Navbar from "./components/Navbar/CustomNavbar/CustomNavbar";
import Error from "./components/Error/Error";
import HI from "./components/Header/Header";
import { HashRouter } from "react-router-dom";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Navbar />
            <HI />
            <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/About" component={About} />
              <Route path="/Contact" component={Contact} />
              <Route component={Error} />
            </Switch>
            <Footer />
          </div>
        </Router>
      </div>
    );
  }
}
