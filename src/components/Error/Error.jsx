import React, { Component } from "react";
import { Jumbotron, Grid, Row, Col, Image, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Error.css";

export default class Error extends Component {
  render() {
    return (
      <Grid>
        <Jumbotron>
          <h2>Tekniskt fel, vänligen besök sidan igen senare.</h2>
          <p />
          <Link to="/">
            <Button bsStyle="primary">Tillbaka till förstasidan</Button>
          </Link>
        </Jumbotron>
      </Grid>
    );
  }
}
