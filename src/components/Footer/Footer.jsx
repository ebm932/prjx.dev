import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Jumbotron, Grid, Row, Col, Button } from "react-bootstrap";
import "./Footer.css";

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer sticky-bottom mt-5">
        <div className="jumbotron2 container-fluid">
          <div className="row">
            <div className="col-md-3">
              <p className="lead footer1"> Fasteko: Lillagatan 4 etc etc</p>
              <p className="lead" />
            </div>
            <div className="col-lg">
              <p>nästa</p>
            </div>
          </div>
          <hr width="100%" />
          <p> © 2019 FEKO förvaltning AB, org.nr. xxx-xxx.</p>
        </div>
      </footer>
    );
  }
}
