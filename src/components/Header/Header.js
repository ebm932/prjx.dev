import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Jumbotron, Grid, Row, Col, button, Form } from "react-bootstrap";
import "./Header.css";
import { Animated } from "react-animated-css";

export default class Header extends Component {
  render() {
    return (
      <div className="home55" animationIn="fadeIn">
        <div class="container">
          <div class="col-md-10">
            <form>
              <div class="row">
                <div class="col-md-6 mb-3">
                  <div className="card w-75">
                    <div className="card-body">
                      <h5 className="card-title">Logga in</h5>
                      <p className="card-text">Med ditt BankId</p>
                      <a href="#" className="knapp1 btn btn-primary">
                        Registrera konto
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
