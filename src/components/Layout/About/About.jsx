import React, { Component } from "react";
import { Grid, Col, Image } from "react-bootstrap";
import "./About.css";

export default class About extends Component {
  render() {
    return (
      <div>
        <Grid>
          <Col xs={12} sm={8} smOffset={2}>
            <h3>Lorem ipsum dolor</h3>
            <p>
              Lorem dolor sit amet, ei sea consequat omittantur persequeris,
              diceret ancillae petentium eam et. Dicant constituam mei et, mel
              veri minim abhorreant cu. Sea ea unum percipitur, quodsi discere
              ex sea. Amet ullum nam no, nec ex tale animal. Id suscipit
              gloriatur vel. Pri ut utamur patrioque hendrerit. Et homero tempor
              gloriatur nam. Elit menandri vix ne. Ex pro vitae ornatus
              incorrupte, viris exerci vix te. Ut sea nostro officiis
              reprehendunt, mei aperiam nostrum delicatissimi ad. Est cu habeo
              saperet inciderint, duo iusto nonumy quaestio an. Cibo integre
              reprehendunt sit ei. Idque adversarium instructior vis eu, ex his
              viris inermis eleifend. Ex pro odio efficiendi, has graece
              suavitate cu, iuvaret tibique minimum vel ex. Perpetua
              mediocritatem ei mei. Ea usu imperdiet mnesarchum. Duo ea antiopam
              accommodare. Mei ut veri illud constituto. Quis assueverit ad vim.
              Eu sea quis illud honestatis, cum ea doctus denique placerat, eam
              appareat urbanitas at. In vis doming deleniti, discere euismod
              qualisque et eum. Putent utroque mentitum est no, ei vim vidisse
              copiosae. Ne tantas timeam pericula eam, suavitate urbanitas
              signiferumque ei sed. Graeci mandamus sea ut, eos cu equidem
              definitiones. Vis brute salutandi te, te labore aliquando vim. Cu
              vim admodum minimum, eam ut quis novum, at cum adversarium
              consectetuer. Graecis delicata expetendis et mel. Populo
              constituto nec eu, ei malis aeterno accumsan mea. Unum referrentur
              vel ei, pro altera eripuit dignissim cu. Stet ullum discere sed
              eu. Pri appetere deserunt ut, facer invenire te cum. Cu ius
              vivendo praesent, nam at sale soleat praesent. Vis ut wisi
              similique, in mucius nominati philosophia mea. Mel ei dicat
              oportere recteque. Per ei augue conclusionemque, appareat
              aliquando inciderint ut his. Ea qui magna ignota, vel assum labore
              placerat te, at scripta salutatus has. Natum nihil incorrupte per
              cu, per te quem animal. Ullum indoctum est an. Vel an viderer
              dissentiunt, modus nonumes te quo. Putent possit eirmod has at. Ne
              vix aeque malorum corpora, ad ipsum graecis cum, eu stet
              suscipiantur interpretaris mea. Ei has mediocrem principes
              philosophia. Eu corpora cotidieque vix, ne posse cetero eum.
              Labores honestatis sit ei, id sit sonet detraxit convenire, ne usu
              inimicus salutatus intellegam. Ut molestie assueverit usu, mei no
              legere dolorum contentiones. Te brute atqui feugait mel. Ridens
              labores per ea, vel eu habeo primis iriure. Vel inimicus
              persequeris in, ne mei euripidis adversarium. Omnis eirmod vim id.
              Vis quis possit accusam in, libris putent moderatius cu mei, ut
              vim nisl civibus reformidans. Eos zril mollis molestiae no, dolor
              impedit vis cu. Per ad simul meliore, qui error saepe ut.
            </p>
            <p>
              Lorem ipsum dolor sit amet, ei sea consequat omittantur
              persequeris, diceret ancillae petentium eam et. Dicant constituam
              mei et, mel veri minim abhorreant cu. Sea ea unum percipitur,
              quodsi discere ex sea. Amet ullum nam no, nec ex tale animal. Id
              suscipit gloriatur vel. Pri ut utamur patrioque hendrerit. Et
              homero tempor gloriatur nam. Elit menandri vix ne. Ex pro vitae
              ornatus incorrupte, viris exerci vix te. Ut sea nostro officiis
              reprehendunt, mei aperiam nostrum delicatissimi ad. Est cu habeo
              saperet inciderint, duo iusto nonumy quaestio an. Cibo integre
              reprehendunt sit ei. Idque adversarium instructior vis eu, ex his
              viris inermis eleifend. Ex pro odio efficiendi, has graece
              suavitate cu, iuvaret tibique minimum vel ex. Perpetua
              mediocritatem ei mei. Ea usu imperdiet mnesarchum. Duo ea antiopam
              accommodare. Mei ut veri illud constituto. Quis assueverit ad vim.
              Eu sea quis illud honestatis, cum ea doctus denique placerat, eam
              appareat urbanitas at. In vis doming deleniti, discere euismod
              qualisque et eum. Putent utroque mentitum est no, ei vim vidisse
              copiosae. Ne tantas timeam pericula eam, suavitate urbanitas
              signiferumque ei sed. Graeci mandamus sea ut, eos cu equidem
              definitiones. Vis brute salutandi te, te labore aliquando vim. Cu
              vim admodum minimum, eam ut quis novum, at cum adversarium
              consectetuer. Graecis delicata expetendis et mel. Populo
              constituto nec eu, ei malis aeterno accumsan mea. Unum referrentur
              vel ei, pro altera eripuit dignissim cu. Stet ullum discere sed
              eu. Pri appetere deserunt ut, facer invenire te cum. Cu ius
              vivendo praesent, nam at sale soleat praesent. Vis ut wisi
              similique, in mucius nominati philosophia mea. Mel ei dicat
              oportere recteque. Per ei augue conclusionemque, appareat
              aliquando inciderint ut his.
            </p>
            <p>
              Lorem ipsum dolor sit amet, ei sea consequat omittantur
              persequeris, diceret ancillae petentium eam et. Dicant constituam
              mei et, mel veri minim abhorreant cu. Sea ea unum percipitur,
              quodsi discere ex sea. Amet ullum nam no, nec ex tale animal. Id
              suscipit gloriatur vel. Pri ut utamur patrioque hendrerit. Et
              homero tempor gloriatur nam. Elit menandri vix ne.
            </p>
            <p>
              Lorem ipsum dolor sit amet, ei sea consequat omittantur
              persequeris, diceret ancillae petentium eam et. Dicant constituam
              mei et, mel veri minim abhorreant cu. Sea ea unum percipitur,
              quodsi discere ex sea. Amet ullum nam no, nec ex tale animal. Id
              suscipit gloriatur vel. Pri ut utamur patrioque hendrerit. Et
              homero tempor gloriatur nam. Elit menandri vix ne.
            </p>
          </Col>
        </Grid>
      </div>
    );
  }
}
