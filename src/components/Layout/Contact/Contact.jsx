import React, { Component } from "react";
import { Grid, Row, Col, Image } from "react-bootstrap";
import "./Contact.css";

export default class Contact extends Component {
  render() {
    return (
      <div>
        <Image src="assets/mountain-man.jpg" className="header-image" />
        <Grid>
          <h2>Contact</h2>
          <Row>
            <Col xs={12} sm={8} className="main-section">
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
            </Col>
            <Col xs={12} sm={4} className="sidebar-section">
              <Image src="assets/dog-people.jpg" />
              <p>
                Lorem ipsum dolor sit amet, ei sea consequat omittantur
                persequeris, diceret ancillae petentium eam et. Dicant
                constituam mei et, mel veri minim abhorreant cu. Sea ea unum
                percipitur, quodsi discere ex sea. Amet ullum nam no, nec ex
                tale animal. Id suscipit gloriatur vel. Pri ut utamur patrioque
                hendrerit. Et homero tempor gloriatur nam. Elit menandri vix ne.
              </p>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
