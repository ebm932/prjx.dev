import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { NavLink, withRouter } from "react-router-dom";
import "./CustomNavbar.css";

class CustomNavbar extends Component {
  render() {
    return (
      <Navbar className="navbar sticky-top navbar-expand-lg navbar-light bg-white">
        <NavLink className="navbar-brand active" to="/">
          <img
            src={require("./img/fasteko.png")}
            className="img-fluid"
            alt="Responsive image"
            width="250cm"
            //classnamne
          />
        </NavLink>
        <button
          className="navbar-toggler ml-auto"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav navbar-default">
            <li className="nav-item dropdown" />
            <NavLink className="nav-item nav-link" to="/contact">
              Teknik
            </NavLink>
            <NavLink className="nav-item nav-link" to="/contact">
              Ekonomi
            </NavLink>
            <NavLink className="nav-item nav-link" to="/contact">
              Konsult
            </NavLink>
            <NavLink className="nav-item nav-link" to="/about">
              Om oss
            </NavLink>
            <NavLink className="nav-item nav-link" to="/contact">
              Kontakt
            </NavLink>
          </ul>
        </div>
      </Navbar>
    );
  }
}
export default withRouter(CustomNavbar);
