import React from "react";
//import asyncBootstrapper from "react-async-bootstrapper";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

//asyncBootstrapper(<App />).then(function() { //Få detta att fungera för att lösa renderingsproblemet i hela hemsidan "Flashen"
// läs mer på https://www.npmjs.com/package/react-bootstrap4-form-validation
ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
//});
